import CartParser from "./CartParser";

let parser;

beforeEach(() => {
	parser = new CartParser();
});

describe("CartParser - unit tests", () => {
	// Add your unit tests here.
	describe("validate function",()=>{
		it("should return error when one header field not correct",()=>{
			const inputData = "Product name,Prise,Quantity\nMorshynsk,10.30,200\nYogurt \"Na Zdorov'ya\",150,17.50"

			const errorArray  = parser.validate(inputData);

			expect(errorArray[0].message).toEqual("Expected header to be named \"Price\" but received Prise.");
		})
		it("should return error when count of body field is lesser from the scheme",()=>{
			const inputData = "Product name,Price,Quantity\nMorshynska,10.30,200\nYogurt \"Na Zdorov'ya\",150"

			const errorArray  = parser.validate(inputData);

			expect(errorArray[0].message).toEqual("Expected row to have 3 cells but received 2.");
		})
		it("should return error when number field not contain positive number",()=>{
			const inputDataNegative = "Product name,Price,Quantity\nMorshynska,10.30,-5\nYogurt \"Na Zdorov'ya\",17.50,150"
			const inputDataString = "Product name,Price,Quantity\nMorshynska,10.30,Five\nYogurt \"Na Zdorov'ya\",17.50,150"
			const inputDataNaN = "Product name,Price,Quantity\nMorshynska,10.30,NaN\nYogurt \"Na Zdorov'ya\",17.50,150"

			const errorArrayNegative  = parser.validate(inputDataNegative);
			const errorArrayString  = parser.validate(inputDataString);
			const errorArrayNaN  = parser.validate(inputDataNaN);

			expect(errorArrayNegative[0].message).toEqual("Expected cell to be a positive number but received \"-5\".");
			expect(errorArrayString[0].message).toEqual("Expected cell to be a positive number but received \"Five\".");
			expect(errorArrayNaN[0].message).toEqual("Expected cell to be a positive number but received \"NaN\".");
		},15000)
		it("should return error when string field is empty",()=>{
			const inputData = "Product name,Price,Quantity\n,200,10.30\nYogurt \"Na Zdorov'ya\",17.50,150"

			const errorArray  = parser.validate(inputData);

			expect(errorArray[0].message).toEqual("Expected cell to be a nonempty string but received \"\".");
		})
		it("should return empty array of error if input data was correct",()=>{
			const inputData = "Product name,Price,Quantity\nMorshynska,200,10.30\nYogurt \"Na Zdorov'ya\",17.50,150"

			const errorArray  = parser.validate(inputData);

			expect(errorArray).toEqual([]);
		})
		it("should return empty array of error if input data has only correct header",()=>{
			const inputData = "Product name,Price,Quantity"

			const errorArray  = parser.validate(inputData);

			expect(errorArray).toEqual([]);
		})
		it("should return several error if input data expectedly should has several errors",()=>{
			const inputData = "Product name,Quantity,Price\nMorshynska,100000\nYogurt \"Na Zdorov'ya\",17.50,150"

			const errorArray  = parser.validate(inputData);

			expect(errorArray[0].message).toEqual("Expected header to be named \"Price\" but received Quantity.");
			expect(errorArray[1].message).toEqual("Expected header to be named \"Quantity\" but received Price.");
			expect(errorArray[2].message).toEqual("Expected row to have 3 cells but received 2.");
		})
	})
	describe("parseLine function",()=>{
		it("should return object with keys from column keys and values from CSV line",()=>{
			const csvLine = "Morshynska,200,10.30";

			const result = parser.parseLine(csvLine)

			expect(result).toMatchObject({
				  name: "Morshynska",
				   price: 200,
				   quantity: 10.3,
				});
		})
		it("should return correct object from empty CSV Line",()=>{
			const csvLine = "";

			const result = parser.parseLine(csvLine)

			expect(result).toMatchObject({
				   name: "",
				   price: NaN,
				   quantity: NaN,
				});
		})
	})
	describe("cartItems function",()=>{
		it("should return total price from filled array items",()=>{
			const cartItems = [{
				name: "Morshynska",
				 price: 200,
				 quantity: 10.3,
			  },
			  {
				name: "Yogurt \"Na Zdorov'ya\"",
				 price: 150,
				 quantity: 17.50,
			  }]

			const result = parser.calcTotal(cartItems)

			expect(result).toEqual(4685);
		})
		it("should return total price from empty array items",()=>{
			const cartItems = []

			const result = parser.calcTotal(cartItems)

			expect(result).toEqual(0);
		})
	})
});

describe("CartParser - integration test", () => {
 	// Add your integration test here.
	 describe("parser module",()=>{
		it("should return object which contain cart items and total price",()=>{
			const path="./samples/test_cart.csv";
			const preduction = {
				items:[{
						name: "Morshynska",
						price: 10.3,
						quantity: 200
					},
					{
						name: "Yogurt \"Na Zdorov'ya\"",
						price: 17.5,
						quantity: 150
					}],
				total:4685
			}

			const result = parser.parse(path)

			expect(result.items).toMatchObject(preduction.items)
			expect(result.total).toEqual(preduction.total)
		})
	})
});